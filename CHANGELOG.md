# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]
### accept tags: Added, Changed, Removed, Deprecated, Removed, Fixed, Security

## [7.0.1] - 2021-09-08
### Changed
- Refactor config/setup.js

## [7.0.0] - 2021-09-08
### Changed
- Server adapters move to npm module

## [6.0.1] - 2021-09-07
### Added
- create CHANGELOG.md