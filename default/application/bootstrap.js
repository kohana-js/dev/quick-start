const { View } = require('@kohanajs/core-mvc');
const { KohanaJS } = require('kohanajs');
const { LiquidView } = require('@kohanajs/mod-liquid-view');
require('@kohanajs/mod-form');

View.DefaultViewClass = LiquidView;

module.exports = {
  modules: [],
};

KohanaJS.initConfig(new Map([
  ['setup', ''],
]));
