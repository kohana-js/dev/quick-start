const { KohanaJS } = require('kohanajs');

KohanaJS.ENV = 'stg';
const Server = require('./Server');

(async () => {
  const s = new Server(8002);
  await s.setup();
  KohanaJS.configForceUpdate = false;
  KohanaJS.config.classes.cache = true;
  //  require("@kohanajs/mod-route-debug");
  await s.listen();
})();
