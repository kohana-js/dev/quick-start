const path = require('path');
const { init, KohanaJS} = require('kohanajs');
// setup KohanaJS path constants
init({
  EXE_PATH:  path.normalize(__dirname),
  APP_PATH:  path.normalize(`${__dirname}/../application`),
  VIEW_PATH: path.normalize(`${__dirname}/../views`),
  MOD_PATH:  path.normalize(`${__dirname}/../application/modules`)
});
// run application/init.js to create routes
require('../application/init');

class Server {
  constructor(port = 8001) {
    this.port = port;
    this.adapter = KohanaJS.config.setup.platform.adapter;
  }

  async setup() {
    this.app = await this.adapter.setup();
  }

  async listen() {
    console.log(KohanaJS.ENV, KohanaJS.config);
    await this.app.listen(this.port);
    console.log(`app listening at ${this.port}`);
  }
}

module.exports = Server;
