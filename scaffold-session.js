const path = require('path');
const { ncp } = require('ncp');
const scaffold = require('./scaffold');
const { unlink } = require('fs/promises');

const init = async dir => {
  await scaffold(dir);

  await new Promise((resolve, reject) =>{
    ncp(path.normalize(`${__dirname}/default-session`), path.normalize(`${dir}`), err => {
      // callback
      if (err) {
        console.log(err);
        reject(err);
        return;
      }
      console.log('file copied');
      resolve();
    });
  });

};

module.exports = async dir => {
  await init(dir);
};
