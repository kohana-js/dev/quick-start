const path = require('path');
const mkdirp = require('mkdirp');
const { ncp } = require('ncp');

const init = async dir => {
  // create folders
  const folders = [
    '/application/classes',
    '/application/classes/controller',
    '/application/classes/model',
    '/views',
    '/views/layout',
    '/views/snippets',
    '/views/templates',
    '/server/tmp',
    '/public',
    '/private/media/css',
  ];

  await Promise.all(folders.map(async folder => {
    const f = path.normalize(dir + folder);
    await mkdirp(f);
    console.log(`create folder: ${f}`);
  }));

  await new Promise((resolve, reject) => {
    ncp(path.normalize(`${__dirname}/default`), path.normalize(`${dir}`), err => {
      // callback
      if (err) {
        console.log(err);
        reject(err);
        return;
      }
      console.log('file copied');
      resolve();
    });
  });
};

module.exports = async dir => {
  await init(dir);
};
