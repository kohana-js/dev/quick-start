const { View } = require('@kohanajs/core-mvc');
const { KohanaJS, ORM } = require('kohanajs');
const { LiquidView } = require('@kohanajs/mod-liquid-view');
const {ORMAdapterSQLite} = require("@kohanajs/mod-database");
require("@kohanajs/mod-crypto");
require("@kohanajs/mod-session");

ORM.defaultAdapter = ORMAdapterSQLite;
View.DefaultViewClass = LiquidView;

module.exports = {
  modules: [],
  system: [],
};

KohanaJS.initConfig(new Map([
  ['cookie', ''],
  ['setup', ''],
]));