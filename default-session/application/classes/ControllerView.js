const {Controller, ControllerMixin} = require('@kohanajs/core-mvc');
const {ControllerMixinMime, ControllerMixinView, ControllerMixinMultipartForm, ControllerMixinDatabase, KohanaJS} = require('kohanajs');
const {DatabaseDriverBetterSQLite3} = require('@kohanajs/mod-database');
const {ControllerMixinSession} = require('@kohanajs/mod-session');

class ControllerMixinStarter extends ControllerMixin{
  static async before(state){
    const client = state.get(ControllerMixin.CLIENT);
    const hostname = client.request.raw.hostname;
    const domain = hostname.split(':')[0];
    //push language, controller, action to layout
    const $_GET = state.get(ControllerMixinMultipartForm.GET_DATA);

    Object.assign(
      state.get(ControllerMixinView.LAYOUT).data,
      {
        domain : domain,
        controller : client.clientName,
        action: client.request.params.action,
        language : client.language,
        language_name : client.languageNames.get(client.language),
        language_abbr : client.languageAbbrs.get(client.language),
        cookieConsent : client.request.cookies['allow-cookie'],
        cookies : JSON.stringify(client.request.cookies),
        utm_source:   encodeURIComponent($_GET['utm_source']   || ""),
        utm_medium:   encodeURIComponent($_GET['utm_medium']   || ""),
        utm_campaign: encodeURIComponent($_GET['utm_campaign'] || ""),
        utm_term:     encodeURIComponent($_GET['utm_term']     || ""),
        utm_content:  encodeURIComponent($_GET['utm_content']  || ""),
        logged_in : client.request.session.logged_in,
      }
    );
  }
}

class ControllerView extends Controller.mixin([ControllerMixinMime, ControllerMixinView, ControllerMixinMultipartForm, ControllerMixinDatabase, ControllerMixinSession, ControllerMixinStarter]){
  constructor(request, controllerName="", layout='layout/default') {
    super(request);
    this.languageNames = new Map([['en', 'English'], ['zh-hans', '简体中文']]);
    this.languageAbbrs = new Map([['en', 'EN'], ['zh-hans', '中文']]);

    this.language = request.params.lang || 'en';
    this.clientName = controllerName;

    this.state.set(ControllerMixinView.LAYOUT_FILE, layout)
    this.state.get(ControllerMixinDatabase.DATABASE_MAP)
      .set('session', `${KohanaJS.config.setup.databaseFolder}/session.sqlite`);

    this.state.set(ControllerMixinDatabase.DATABASE_DRIVER, DatabaseDriverBetterSQLite3);
  }

  async serverError(err){
    this.setErrorTemplate(`templates/${this.language}/error`, {
      message: String(err).replace('Error:', ''),
      title: 'Error',
      language: this.language,
    });

    this.body = await this.get(ControllerMixinView.ERROR_TEMPLATE).render();
    await super.serverError(err);

  }
}

module.exports = ControllerView;