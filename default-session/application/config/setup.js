const {KohanaJS} = require('kohanajs');

module.exports = {
  notFound: '/pages/404',
  databaseFolder: `${KohanaJS.EXE_PATH}/../db`,
}