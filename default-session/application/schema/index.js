const path = require('path');
const {build} = require('kohanajs-start');

build(
  `${__dirname}/session.graphql`,
  ``,
  `${__dirname}/exports/session.sql`,
  `${__dirname}/../default/db/session.sqlite`,
  path.normalize(`${__dirname}/classes/model`),
  true
)

build(
  `${__dirname}/example.graphql`,
  `${__dirname}/example.js`,
  `${__dirname}/exports/example.sql`,
  `${__dirname}/default/db/example.sqlite`,
  `${__dirname}/../server/application/classes/model`
)