const path = require('path');
const { ncp } = require('ncp');
const { unlink } = require('fs/promises');
const scaffold = require('./scaffold');

const init = async dir => {
  await scaffold(dir);

  await new Promise((resolve, reject) => {
    ncp(path.normalize(`${__dirname}/default-admin`), path.normalize(`${dir}`), err => {
      // callback
      if (err) {
        console.log(err);
        reject(err);
        return;
      }
      console.log('file copied');
      resolve();
    });
  });

  await unlink(path.normalize(`${dir}/views/templates/home.liquid`));
  await unlink(path.normalize(`${dir}/application/classes/controller/Home.js`));
};

module.exports = async dir => {
  await init(dir);
};
