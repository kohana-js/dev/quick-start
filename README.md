# Quick start

KohanaJS is a Node.js ORM framework using Fastify and Better SQLite3 as default web server and database.

###Installing
````
npm i kohanajs-start --save
````

Create folder structure, bootstrap.js and server files.
````
npx kohanajs-scaffold
````

###Let's start the server.
````
cd server
node fastify.js
````

Test your installation by opening http://localhost:8081