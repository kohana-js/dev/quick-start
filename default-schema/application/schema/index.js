const { build } = require('kohanajs-start');

build(
  `${__dirname}/example.graphql`,
  '',
  `${__dirname}/exports/example.sql`,
  `${__dirname}/default/db/example.sqlite`,
  `${__dirname}/../server/application/classes/model`,
);
