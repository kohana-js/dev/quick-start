const path = require('path');
const mkdirp = require('mkdirp');
const fs = require('fs');

const init = async dir => {
  // create folders
  const folders = [
    '/db',
    '/schema',
    '/schema/exports',
  ];

  await Promise.all(folders.map(async folder => {
    const f = path.normalize(dir + folder);
    await mkdirp(f);
    console.log(`create folder: ${f}`);
  }));

  setTimeout(() => {
    fs.copyFileSync(path.normalize(`${__dirname}/GraphQL/_header.graphql`), path.normalize(`${dir}/schema/_header.graphql`));
    fs.copyFileSync(path.normalize(`${__dirname}/GraphQL/_interfaces.graphql`), path.normalize(`${dir}/schema/_interfaces.graphql`));
    fs.copyFileSync(path.normalize(`${__dirname}/default-schema/schema/example.graphql`), path.normalize(`${dir}/schema/example.graphql`));
    fs.copyFileSync(path.normalize(`${__dirname}/default-schema/schema/index.js`), path.normalize(`${dir}/schema/index.js`));
  }, 100);
};

module.exports = async dir => {
  await init(dir);
};
