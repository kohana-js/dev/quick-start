const { KohanaJS } = require('kohanajs');

KohanaJS.ENV = 'uat';
const Server = require('./Server');

(async () => {
  const s = new Server(8004);
  await s.setup();
  //  require("@kohanajs/mod-route-debug");
  await s.listen();
})();
