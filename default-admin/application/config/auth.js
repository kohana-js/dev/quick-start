const path = require('path');
const {KohanaJS} = require('kohanajs');

module.exports = {
  databasePath: path.normalize(KohanaJS.EXE_PATH + '/../database'),
};
