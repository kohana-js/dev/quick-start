const { View } = require('@kohanajs/core-mvc');
const { KohanaJS, ORM } = require('kohanajs');
const { LiquidView } = require('@kohanajs/mod-liquid-view');
const {ORMAdapterSQLite} = require("@kohanajs/mod-database");
require('@kohanajs/mod-form');
require("@kohanajs/mod-crypto");
require("@kohanajs/mod-session");
require("@kohanajs/mod-signup");
require("@kohanajs/mod-admin");
require("@kohanajs/mod-admin-view");

ORM.defaultAdapter = ORMAdapterSQLite;
View.DefaultViewClass = LiquidView;

module.exports = {
  modules: [],
};

KohanaJS.initConfig(new Map([
  ['setup', ''],
]));
