const path = require('path');
const {build} = require('kohanajs-start');

build(
  `${__dirname}/session.graphql`,
  ``,
  `${__dirname}/exports/session.sql`,
  `${__dirname}/default/db/session.sqlite`,
  path.normalize(`${__dirname}/classes/model`),
  true
)

build(
  `${__dirname}/admin.graphql`,
  `${__dirname}/admin.js`,
  `${__dirname}/exports/admin.sql`,
  `${__dirname}/default/db/admin.sqlite`,
  path.normalize(`${__dirname}/classes/model`),
  true
)