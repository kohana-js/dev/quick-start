module.exports = {
  build: require('./build'),
  scaffold: require('./scaffold'),
  schema: require('./schema'),
};
